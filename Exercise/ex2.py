
# A comment, this is so you can read your program Later.
#Anything after the # is ignored
print "I could have code like this." # and the comment after iut is ignored
#You can also use a comment to "disabel" or comment out a piece of cade:
# print "this will not run."
print "This will run"
